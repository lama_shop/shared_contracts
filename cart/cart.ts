import type { ProductI } from '../product/product'

export type ProductCart = Pick<ProductI, 'title' | 'description' | 'finalPrice' | 'regularPrice' | 'discountOff' | 'image'>

export interface CartI {
  id: string
  items: CartItemI[]
  createdAt: Date
}

export interface CreateEmptyCartI {
  cartId: string
}

export interface CartItemI {
  id: string
  quantity: number
  createdAt: String,
  product: ProductI
}

export interface AddCartItem {
  productSku: string
  quantity: number
}

export interface UpdateCartItem {
  productId: string
  quantity: number
}

export interface RemoveCartItem {
  productSku: string
}

export interface AddressI {
  firstName: string,
  lastName: string,
  email: string,
  phoneNumber: string,
  region: string,
  country: string,
  address: string
  postcode: string,
  city: string
}

export type BillingAddressI = AddressI
export type ShippingAddressI = AddressI

export interface CompanyAddressI {
  name: string,
  taxNumber: string
}

export interface DeliverMethodI {
  label: string,
  price: {
    gross: number,
    net: number
  }
  iconName: string,
  value: string
}

export interface PaymentMethodI {
  label: string,
  value: string,
  iconName: string,
}
