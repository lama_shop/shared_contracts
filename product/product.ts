export interface ProductI {
  sku: string,
  title: string,
  description: string,
  regularPrice: Price,
  finalPrice: Price,
  discountOff: Price,
  image: {
    alt: string,
    url: string
  }
}

export interface Price {
  gross: number,
  net: number
}
